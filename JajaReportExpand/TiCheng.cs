﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

 
  public   class TiCheng
    {

        public double  decBegin { set; get; }
        public double  decEnd { set; get; }

        public double  decTiCheng { set; get; }

    public List<TiCheng> List { set; get; }

    public TiCheng()
    {
        this.List = new List<TiCheng> ();
    }

    public  void LoadFile( string FileName)
    {
        IniHelper ini = new IniHelper(FileName );
        System.Collections.Specialized.StringCollection sections = new System.Collections.Specialized.StringCollection();

        ini.ReadSections(sections);

        
        List .Clear();

        int n = 1;
        foreach (var s in sections)
        {

            TiCheng tc = new TiCheng();
            tc.decBegin = double .Parse(ini.ReadIniData("条件" + n.ToString(), "Begin"));
            tc.decEnd = double.Parse(ini.ReadIniData("条件" + n.ToString(), "End"));
            tc.decTiCheng = double.Parse(ini.ReadIniData("条件" + n.ToString(), "TiChengMoney"));
            List .Add(tc);

            n += 1;
        }
    }

        public double GetTiChengMoney( double  Money, List <TiCheng > tcList)
        {

        double tcmoney = 0;
            foreach (var t in tcList )

            {
                if (Money>=t.decBegin && Money <=t.decEnd )
                {
                    tcmoney = t.decTiCheng;
                }



            }
            return tcmoney;

        }

    }
 
