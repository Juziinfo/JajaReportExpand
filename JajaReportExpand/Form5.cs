﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;

using System.Data.SqlClient;
using CCWin;

namespace JajaRun
{
    public partial class Form5 : CCWin.CCSkinMain 
    {
        public Form5()
        {
            InitializeComponent();
        }

        private void Form5_Load(object sender, EventArgs e)
        {
            string fileName=Application.StartupPath + "\\TiChengSetting.ini" ;
            TiCheng tc = new TiCheng();
            tc.LoadFile(fileName);

            TiChengList = tc.List;
            dgv.DataSource = tc.List;
        }


        List<TiCheng> TiChengList = new List<TiCheng>() ;
        private void btn_OK_Click(object sender, EventArgs e)
        {
            int n = 1;
            foreach (var tc in TiChengList)
            {

                WriterDbCon("条件" + n.ToString() ,tc);
                n += 1;
            }

            this.Close();



        }

        private void WriterDbCon(string section, TiCheng tc)
        {
            //连接成功！写入配置文件
            IniHelper ini = new IniHelper(Application.StartupPath + "\\TiChengSetting.ini");
            ini.WriteIniData(section, "Begin", tc.decBegin .ToString (), ini.IniFilePath);
            ini.WriteIniData(section, "End", tc.decEnd .ToString (), ini.IniFilePath);
            ini.WriteIniData(section, "TiChengMoney", tc.decTiCheng .ToString (), ini.IniFilePath);
      
        }

        private void skinButton1_Click(object sender, EventArgs e)
        {
            TiCheng tc = new TiCheng();
            tc.decBegin = Convert.ToDouble(num_Begin.Value);
            tc.decEnd = Convert.ToDouble (num_End.Value);
            tc.decTiCheng =Convert.ToDouble (skinNumericUpDown1.Value);

            if (!TiChengList.Exists(t => tc.decBegin <= t.decEnd && tc.decBegin >= t.decBegin))
            {
                TiChengList.Add(tc);
            }

            dgv.DataSource = new List<TiCheng>();
            dgv.DataSource = TiChengList;

        }

        private void btn_clear_Click(object sender, EventArgs e)
        {
            IniHelper ini = new IniHelper(Application.StartupPath + "\\TiChengSetting.ini");

            int n = 1;
            foreach (var t in TiChengList )
            {
                ini.ClearSection("条件" + n.ToString());
                n += 1;
            }

            TiChengList.Clear();
            dgv.DataSource = new List<TiCheng>();
            dgv.DataSource = TiChengList;

 

        }

        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        
    }
}
