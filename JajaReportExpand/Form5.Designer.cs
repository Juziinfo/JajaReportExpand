﻿namespace JajaRun
{
    partial class Form5
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btn_OK = new CCWin.SkinControl.SkinButton();
            this.btn_Cancel = new CCWin.SkinControl.SkinButton();
            this.label1 = new System.Windows.Forms.Label();
            this.num_Begin = new CCWin.SkinControl.SkinNumericUpDown();
            this.num_End = new CCWin.SkinControl.SkinNumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.skinNumericUpDown1 = new CCWin.SkinControl.SkinNumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.btn_Add = new CCWin.SkinControl.SkinButton();
            this.btn_clear = new CCWin.SkinControl.SkinButton();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.提成金额 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.num_Begin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_End)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.skinNumericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_OK
            // 
            this.btn_OK.BackColor = System.Drawing.Color.Transparent;
            this.btn_OK.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(175)))), ((int)(((byte)(154)))));
            this.btn_OK.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(175)))), ((int)(((byte)(154)))));
            this.btn_OK.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_OK.DownBack = null;
            this.btn_OK.DownBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(178)))), ((int)(((byte)(255)))));
            this.btn_OK.ForeColor = System.Drawing.Color.White;
            this.btn_OK.InnerBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(157)))), ((int)(((byte)(154)))));
            this.btn_OK.IsDrawBorder = false;
            this.btn_OK.IsDrawGlass = false;
            this.btn_OK.IsEnabledDraw = false;
            this.btn_OK.Location = new System.Drawing.Point(253, 291);
            this.btn_OK.MouseBack = null;
            this.btn_OK.MouseBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(178)))), ((int)(((byte)(255)))));
            this.btn_OK.Name = "btn_OK";
            this.btn_OK.NormlBack = null;
            this.btn_OK.Size = new System.Drawing.Size(76, 32);
            this.btn_OK.TabIndex = 3;
            this.btn_OK.Text = "保存";
            this.btn_OK.UseVisualStyleBackColor = false;
            this.btn_OK.Click += new System.EventHandler(this.btn_OK_Click);
            // 
            // btn_Cancel
            // 
            this.btn_Cancel.BackColor = System.Drawing.Color.Transparent;
            this.btn_Cancel.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(175)))), ((int)(((byte)(154)))));
            this.btn_Cancel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(175)))), ((int)(((byte)(154)))));
            this.btn_Cancel.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_Cancel.DownBack = null;
            this.btn_Cancel.DownBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(178)))), ((int)(((byte)(255)))));
            this.btn_Cancel.ForeColor = System.Drawing.Color.White;
            this.btn_Cancel.InnerBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(157)))), ((int)(((byte)(154)))));
            this.btn_Cancel.IsDrawBorder = false;
            this.btn_Cancel.IsDrawGlass = false;
            this.btn_Cancel.IsEnabledDraw = false;
            this.btn_Cancel.Location = new System.Drawing.Point(346, 291);
            this.btn_Cancel.MouseBack = null;
            this.btn_Cancel.MouseBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(178)))), ((int)(((byte)(255)))));
            this.btn_Cancel.Name = "btn_Cancel";
            this.btn_Cancel.NormlBack = null;
            this.btn_Cancel.Size = new System.Drawing.Size(77, 32);
            this.btn_Cancel.TabIndex = 4;
            this.btn_Cancel.Text = "取消";
            this.btn_Cancel.UseVisualStyleBackColor = false;
            this.btn_Cancel.Click += new System.EventHandler(this.btn_Cancel_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(44, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 21);
            this.label1.TabIndex = 1;
            this.label1.Text = "当消费金额从";
            // 
            // num_Begin
            // 
            this.num_Begin.Location = new System.Drawing.Point(156, 35);
            this.num_Begin.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.num_Begin.Name = "num_Begin";
            this.num_Begin.Size = new System.Drawing.Size(87, 23);
            this.num_Begin.TabIndex = 5;
            // 
            // num_End
            // 
            this.num_End.Location = new System.Drawing.Point(281, 35);
            this.num_End.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.num_End.Name = "num_End";
            this.num_End.Size = new System.Drawing.Size(87, 23);
            this.num_End.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(249, 36);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(26, 21);
            this.label5.TabIndex = 1;
            this.label5.Text = "到";
            // 
            // skinNumericUpDown1
            // 
            this.skinNumericUpDown1.Location = new System.Drawing.Point(422, 35);
            this.skinNumericUpDown1.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.skinNumericUpDown1.Name = "skinNumericUpDown1";
            this.skinNumericUpDown1.Size = new System.Drawing.Size(87, 23);
            this.skinNumericUpDown1.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(374, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 21);
            this.label2.TabIndex = 1;
            this.label2.Text = "提成";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(515, 36);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 21);
            this.label3.TabIndex = 1;
            this.label3.Text = "元。";
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.提成金额});
            this.dgv.Location = new System.Drawing.Point(39, 113);
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowTemplate.Height = 23;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(594, 172);
            this.dgv.TabIndex = 6;
            // 
            // btn_Add
            // 
            this.btn_Add.BackColor = System.Drawing.Color.Transparent;
            this.btn_Add.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(175)))), ((int)(((byte)(154)))));
            this.btn_Add.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(175)))), ((int)(((byte)(154)))));
            this.btn_Add.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_Add.DownBack = null;
            this.btn_Add.DownBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(178)))), ((int)(((byte)(255)))));
            this.btn_Add.ForeColor = System.Drawing.Color.White;
            this.btn_Add.InnerBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(157)))), ((int)(((byte)(154)))));
            this.btn_Add.IsDrawBorder = false;
            this.btn_Add.IsDrawGlass = false;
            this.btn_Add.IsEnabledDraw = false;
            this.btn_Add.Location = new System.Drawing.Point(485, 79);
            this.btn_Add.MouseBack = null;
            this.btn_Add.MouseBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(178)))), ((int)(((byte)(255)))));
            this.btn_Add.Name = "btn_Add";
            this.btn_Add.NormlBack = null;
            this.btn_Add.Size = new System.Drawing.Size(62, 28);
            this.btn_Add.TabIndex = 7;
            this.btn_Add.Text = "添加";
            this.btn_Add.UseVisualStyleBackColor = false;
            this.btn_Add.Click += new System.EventHandler(this.skinButton1_Click);
            // 
            // btn_clear
            // 
            this.btn_clear.BackColor = System.Drawing.Color.Transparent;
            this.btn_clear.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(175)))), ((int)(((byte)(154)))));
            this.btn_clear.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(175)))), ((int)(((byte)(154)))));
            this.btn_clear.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_clear.DownBack = null;
            this.btn_clear.DownBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(178)))), ((int)(((byte)(255)))));
            this.btn_clear.ForeColor = System.Drawing.Color.White;
            this.btn_clear.InnerBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(157)))), ((int)(((byte)(154)))));
            this.btn_clear.IsDrawBorder = false;
            this.btn_clear.IsDrawGlass = false;
            this.btn_clear.IsEnabledDraw = false;
            this.btn_clear.Location = new System.Drawing.Point(553, 79);
            this.btn_clear.MouseBack = null;
            this.btn_clear.MouseBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(178)))), ((int)(((byte)(255)))));
            this.btn_clear.Name = "btn_clear";
            this.btn_clear.NormlBack = null;
            this.btn_clear.Size = new System.Drawing.Size(62, 28);
            this.btn_clear.TabIndex = 7;
            this.btn_clear.Text = "重置";
            this.btn_clear.UseVisualStyleBackColor = false;
            this.btn_clear.Click += new System.EventHandler(this.btn_clear_Click);
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "decBegin";
            this.Column1.HeaderText = "起始金额";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "decEnd";
            this.Column2.HeaderText = "结束金额";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // 提成金额
            // 
            this.提成金额.DataPropertyName = "decTiCheng";
            this.提成金额.HeaderText = "提成金额";
            this.提成金额.Name = "提成金额";
            this.提成金额.ReadOnly = true;
            // 
            // Form5
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(660, 347);
            this.Controls.Add(this.btn_clear);
            this.Controls.Add(this.btn_Add);
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.skinNumericUpDown1);
            this.Controls.Add(this.num_End);
            this.Controls.Add(this.num_Begin);
            this.Controls.Add(this.btn_Cancel);
            this.Controls.Add(this.btn_OK);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "Form5";
            this.Text = "设置提成条件";
            this.Load += new System.EventHandler(this.Form5_Load);
            ((System.ComponentModel.ISupportInitialize)(this.num_Begin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_End)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.skinNumericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private CCWin.SkinControl.SkinButton btn_OK;
        private CCWin.SkinControl.SkinButton btn_Cancel;
        private System.Windows.Forms.Label label1;
        private CCWin.SkinControl.SkinNumericUpDown num_Begin;
        private CCWin.SkinControl.SkinNumericUpDown num_End;
        private System.Windows.Forms.Label label5;
        private CCWin.SkinControl.SkinNumericUpDown skinNumericUpDown1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dgv;
        private CCWin.SkinControl.SkinButton btn_Add;
        private CCWin.SkinControl.SkinButton btn_clear;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn 提成金额;
    }
}