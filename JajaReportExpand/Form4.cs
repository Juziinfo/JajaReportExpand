﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CCWin;

namespace JajaRun
{
    public partial class Form4 : CCWin.CCSkinMain 
    {
        public Form4()
        {
            InitializeComponent();
        }


        string MyZhangtao;
        private void Form4_Load(object sender, EventArgs e)
        {
            
            dgv.AutoGenerateColumns = false;
            dgv.DataSource = new List<Model.mn_Menu>();
            dgv.DataSource = MenuList;
            //获取当前账套的名称
            IniHelper inihelp = new IniHelper(Application.StartupPath + "\\ConDB.ini");
              MyZhangtao = inihelp.ReadIniData("当前账套", "账套", "新建账套");

            //MessageBox.Show(MyZhangtao );

            //加载已设置数据
            IniHelper ini = new IniHelper(Application.StartupPath + "\\PaiChuMenu.ini");

            System.Collections.Specialized.NameValueCollection Values = new System.Collections.Specialized.NameValueCollection();
            ini.ReadSectionValues(MyZhangtao , Values);

            foreach (string MenuID in Values )
            {
                PaiChuMenuList.Add(MenuList.Find(m => m.MenuID == MenuID));
                

            }

            dgv_Paichu.AutoGenerateColumns = false;
            dgv_Paichu.DataSource = new List<Model.mn_Menu>();
            dgv_Paichu.DataSource = PaiChuMenuList;


            loadBigClass();
            loadSmallClass();
        }

        //填充大类与小类
        private void loadBigClass()
        { 

            List <Model.mn_BigClass > bigClassList = Model.mn_BigClass.Fetch("WHERE 1=1");
            Model.mn_BigClass bc = new Model.mn_BigClass();
            bc.BigClassID = "";
            bc.BigClassName = "";
            bigClassList.Insert (0,bc);

            cmb_BigClass.DataSource = bigClassList;
            cmb_BigClass.DisplayMember = "BigClassName";
            cmb_BigClass.ValueMember = "BigClassID";

        }
        private void loadSmallClass()
        {
            List<Model.mn_SmallClass>  smallClassList = Model.mn_SmallClass.Fetch("WHERE BigClassID=@0",cmb_BigClass.SelectedValue.ToString ());
            Model.mn_SmallClass sc = new Model.mn_SmallClass();
            sc.SmallClassID  = "";
            sc.SmallClassName  = "";
            smallClassList .Insert(0, sc);


            cmb_SmallClass.DataSource = smallClassList;
            cmb_SmallClass.DisplayMember = "SmallClassName";
            cmb_SmallClass.ValueMember = "SmallClassID";
        }


        List<Model.mn_Menu> MenuList = Model.mn_Menu.Fetch("select MenuID,MenuName,m.BianMa,MenuUnit,m.BigClassID,m.SmallClassID,NormalPrice,MemberPrice,BigClassName,SmallClassName from mn_menu as m Join mn_BigClass as b on m.BigClassID=b.BigClassID Join mn_SmallClass as s on m.SmallClassID=s.SmallClassID ");

        private void btn_Querty_Click(object sender, EventArgs e)
        {
            string FileName = Application.StartupPath + "\\PaiChuMenu.ini";
            IniHelper inihelper = new IniHelper(FileName);

            if (PaiChuMenuList.Count > 0)
            {

                foreach (var m in PaiChuMenuList)
                {
                    inihelper.WriteIniData(MyZhangtao, m.MenuID, m.MenuName, FileName);
                   
                }
              


            }
            else
            {
                inihelper.ClearSection(MyZhangtao);

            }

            MessageBoxEx.Show("保存成功！");
        }

      

        List<Model.mn_Menu> PaiChuMenuList= new List<Model.mn_Menu> ();
      


        
        private void skinWaterTextBox1_TextChanged(object sender, EventArgs e)
       {
            string txt = skinWaterTextBox1.Text;

            //重新加载dgv
            dgv.DataSource = new List<Model.mn_Menu>();
            dgv.DataSource = MenuList.FindAll(m => m.MenuID.Contains(txt) || m.MenuName.Contains(txt) || m.BianMa.Contains(txt.ToUpper()));

       

      
             
        }

        private void skinButton1_Click(object sender, EventArgs e)
        {
            //this.Close();

            IniHelper ini = new IniHelper(Application.StartupPath + "\\PaiChuMenu.ini");
            System.Collections.Specialized.StringCollection sc =new System.Collections.Specialized.StringCollection ();
            System.Collections.Specialized.NameValueCollection  values = new System.Collections.Specialized.NameValueCollection();

            ini.ReadSectionValues ( "PaiChuMenus", values);

            foreach (var s in values)
            {

                MessageBoxEx.Show(s.ToString() );
            }
        }

        private void cmb_BigClass_SelectedValueChanged(object sender, EventArgs e)
        {

            try
            {
                if (cmb_BigClass.SelectedValue != null)
                {
                    loadSmallClass();
                }
            }
            catch (Exception)
            {

                 
            }
           
         
        }

        private void cmb_SmallClass_SelectedIndexChanged(object sender, EventArgs e)
        {
            dgv.DataSource = new List<Model.mn_Menu>();
            if (cmb_SmallClass.SelectedValue == null || cmb_SmallClass.Text.Trim() == "")
            {
               

                if (cmb_BigClass.SelectedValue == null || cmb_BigClass.Text.Trim() == "")
                {
                    dgv.DataSource = MenuList;
                }
                else
                {

                    dgv.DataSource = MenuList.FindAll(m => m.BigClassID == cmb_BigClass.SelectedValue.ToString());
                }

            }

            else
            {
                dgv.DataSource = MenuList.FindAll(m => m.SmallClassID  == cmb_SmallClass .SelectedValue.ToString());
            }
        }

        private void skinButton2_Click(object sender, EventArgs e)
        {
            PaiChuMenuList.Clear();
            dgv_Paichu.DataSource = new List<Model.mn_Menu>();
        dgv_Paichu .DataSource = PaiChuMenuList;
        }




 

        private void dgv_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

          
            string menuid = dgv.Rows[e.RowIndex].Cells[1].Value.ToString();

            //添加到排除列表中

            Model.mn_Menu menu = MenuList.Find(m => m.MenuID == menuid);
            //if (PaiChuMenuList == null)
            //{
            //MessageBox.Show(PaiChuMenuList.Count.ToString ());

            //}
            //if ( PaiChuMenuList.Count ==0 || !PaiChuMenuList.Exists(m => m.MenuID == menu.MenuID))
            //{
            //    PaiChuMenuList.Add(menu);
            //}

            bool IsExists = false;
            foreach (var m in PaiChuMenuList )
            {
                if (m.MenuID == menu.MenuID)
                {
                    IsExists = true;
                }
                
            }

            if (!IsExists)
            {
                PaiChuMenuList.Add(menu);
            }

            //MessageBox.Show(PaiChuMenuList.Count .ToString ());
            //加载排除菜品列表
            dgv_Paichu.AutoGenerateColumns = false;
            dgv_Paichu.DataSource = new List<Model.mn_Menu>();
            dgv_Paichu.DataSource = PaiChuMenuList;
        }
    }
}
