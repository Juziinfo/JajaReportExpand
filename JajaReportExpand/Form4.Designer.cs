﻿namespace JajaRun
{
    partial class Form4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.skinButton2 = new CCWin.SkinControl.SkinButton();
            this.skinButton1 = new CCWin.SkinControl.SkinButton();
            this.btn_Querty = new CCWin.SkinControl.SkinButton();
            this.cmb_SmallClass = new CCWin.SkinControl.SkinComboBox();
            this.cmb_BigClass = new CCWin.SkinControl.SkinComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.skinWaterTextBox1 = new CCWin.SkinControl.SkinWaterTextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.dgv_Paichu = new System.Windows.Forms.DataGridView();
            this.MenuID2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MenuName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Check = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.MenuID1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MenuName1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BianMa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BigClassName1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SmallClassName1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Unit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NormalPrice1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MemberPrice1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.panel1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Paichu)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.skinButton2);
            this.groupBox1.Controls.Add(this.skinButton1);
            this.groupBox1.Controls.Add(this.btn_Querty);
            this.groupBox1.Controls.Add(this.cmb_SmallClass);
            this.groupBox1.Controls.Add(this.cmb_BigClass);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(221, 0);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(6, 3, 3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(568, 94);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "菜品类别检索";
            // 
            // skinButton2
            // 
            this.skinButton2.BackColor = System.Drawing.Color.Transparent;
            this.skinButton2.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(175)))), ((int)(((byte)(154)))));
            this.skinButton2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(175)))), ((int)(((byte)(154)))));
            this.skinButton2.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinButton2.DownBack = null;
            this.skinButton2.DownBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(178)))), ((int)(((byte)(255)))));
            this.skinButton2.ForeColor = System.Drawing.Color.White;
            this.skinButton2.InnerBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(157)))), ((int)(((byte)(154)))));
            this.skinButton2.IsDrawBorder = false;
            this.skinButton2.IsDrawGlass = false;
            this.skinButton2.IsEnabledDraw = false;
            this.skinButton2.Location = new System.Drawing.Point(484, 66);
            this.skinButton2.MouseBack = null;
            this.skinButton2.MouseBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(178)))), ((int)(((byte)(255)))));
            this.skinButton2.Name = "skinButton2";
            this.skinButton2.NormlBack = null;
            this.skinButton2.Size = new System.Drawing.Size(78, 22);
            this.skinButton2.TabIndex = 5;
            this.skinButton2.Text = "清空列表";
            this.skinButton2.UseVisualStyleBackColor = false;
            this.skinButton2.Click += new System.EventHandler(this.skinButton2_Click);
            // 
            // skinButton1
            // 
            this.skinButton1.BackColor = System.Drawing.Color.Transparent;
            this.skinButton1.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(175)))), ((int)(((byte)(154)))));
            this.skinButton1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(175)))), ((int)(((byte)(154)))));
            this.skinButton1.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinButton1.DownBack = null;
            this.skinButton1.DownBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(178)))), ((int)(((byte)(255)))));
            this.skinButton1.ForeColor = System.Drawing.Color.White;
            this.skinButton1.InnerBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(157)))), ((int)(((byte)(154)))));
            this.skinButton1.IsDrawBorder = false;
            this.skinButton1.IsDrawGlass = false;
            this.skinButton1.IsEnabledDraw = false;
            this.skinButton1.Location = new System.Drawing.Point(212, 46);
            this.skinButton1.MouseBack = null;
            this.skinButton1.MouseBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(178)))), ((int)(((byte)(255)))));
            this.skinButton1.Name = "skinButton1";
            this.skinButton1.NormlBack = null;
            this.skinButton1.Size = new System.Drawing.Size(84, 30);
            this.skinButton1.TabIndex = 5;
            this.skinButton1.Text = "取消";
            this.skinButton1.UseVisualStyleBackColor = false;
            this.skinButton1.Click += new System.EventHandler(this.skinButton1_Click);
            // 
            // btn_Querty
            // 
            this.btn_Querty.BackColor = System.Drawing.Color.Transparent;
            this.btn_Querty.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(175)))), ((int)(((byte)(154)))));
            this.btn_Querty.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(175)))), ((int)(((byte)(154)))));
            this.btn_Querty.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_Querty.DownBack = null;
            this.btn_Querty.DownBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(178)))), ((int)(((byte)(255)))));
            this.btn_Querty.ForeColor = System.Drawing.Color.White;
            this.btn_Querty.InnerBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(157)))), ((int)(((byte)(154)))));
            this.btn_Querty.IsDrawBorder = false;
            this.btn_Querty.IsDrawGlass = false;
            this.btn_Querty.IsEnabledDraw = false;
            this.btn_Querty.Location = new System.Drawing.Point(212, 14);
            this.btn_Querty.MouseBack = null;
            this.btn_Querty.MouseBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(178)))), ((int)(((byte)(255)))));
            this.btn_Querty.Name = "btn_Querty";
            this.btn_Querty.NormlBack = null;
            this.btn_Querty.Size = new System.Drawing.Size(84, 30);
            this.btn_Querty.TabIndex = 5;
            this.btn_Querty.Text = "确定";
            this.btn_Querty.UseVisualStyleBackColor = false;
            this.btn_Querty.Click += new System.EventHandler(this.btn_Querty_Click);
            // 
            // cmb_SmallClass
            // 
            this.cmb_SmallClass.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cmb_SmallClass.FormattingEnabled = true;
            this.cmb_SmallClass.Location = new System.Drawing.Point(66, 50);
            this.cmb_SmallClass.Name = "cmb_SmallClass";
            this.cmb_SmallClass.Size = new System.Drawing.Size(121, 22);
            this.cmb_SmallClass.TabIndex = 1;
            this.cmb_SmallClass.WaterText = "";
            this.cmb_SmallClass.SelectedIndexChanged += new System.EventHandler(this.cmb_SmallClass_SelectedIndexChanged);
            // 
            // cmb_BigClass
            // 
            this.cmb_BigClass.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cmb_BigClass.FormattingEnabled = true;
            this.cmb_BigClass.Location = new System.Drawing.Point(66, 18);
            this.cmb_BigClass.Name = "cmb_BigClass";
            this.cmb_BigClass.Size = new System.Drawing.Size(121, 22);
            this.cmb_BigClass.TabIndex = 1;
            this.cmb_BigClass.WaterText = "";
            this.cmb_BigClass.SelectedValueChanged += new System.EventHandler(this.cmb_BigClass_SelectedValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "菜品小类";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "菜品大类";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.skinWaterTextBox1);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 3, 6, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(221, 94);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "菜品模糊检索";
            // 
            // skinWaterTextBox1
            // 
            this.skinWaterTextBox1.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinWaterTextBox1.Location = new System.Drawing.Point(6, 35);
            this.skinWaterTextBox1.Name = "skinWaterTextBox1";
            this.skinWaterTextBox1.Size = new System.Drawing.Size(206, 26);
            this.skinWaterTextBox1.TabIndex = 1;
            this.skinWaterTextBox1.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.skinWaterTextBox1.WaterText = "菜品编号、名称、拼音简码";
            this.skinWaterTextBox1.TextChanged += new System.EventHandler(this.skinWaterTextBox1_TextChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dgv);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(4, 122);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(543, 270);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "菜品选择（选中菜品不参与提成统计）";
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Check,
            this.MenuID1,
            this.MenuName1,
            this.BianMa,
            this.BigClassName1,
            this.SmallClassName1,
            this.Unit,
            this.NormalPrice1,
            this.MemberPrice1});
            this.dgv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv.Location = new System.Drawing.Point(3, 17);
            this.dgv.Name = "dgv";
            this.dgv.RowTemplate.Height = 23;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(537, 250);
            this.dgv.TabIndex = 1;
            this.dgv.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellContentClick);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(4, 28);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(789, 94);
            this.panel1.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(4, 392);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(543, 37);
            this.panel2.TabIndex = 2;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.dgv_Paichu);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Right;
            this.groupBox4.Location = new System.Drawing.Point(547, 122);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(246, 307);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "不参与统计的菜品";
            // 
            // dgv_Paichu
            // 
            this.dgv_Paichu.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_Paichu.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_Paichu.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MenuID2,
            this.MenuName});
            this.dgv_Paichu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_Paichu.Location = new System.Drawing.Point(3, 17);
            this.dgv_Paichu.Name = "dgv_Paichu";
            this.dgv_Paichu.RowTemplate.Height = 23;
            this.dgv_Paichu.Size = new System.Drawing.Size(240, 287);
            this.dgv_Paichu.TabIndex = 2;
            // 
            // MenuID2
            // 
            this.MenuID2.DataPropertyName = "MenuID";
            this.MenuID2.HeaderText = "菜品编号";
            this.MenuID2.Name = "MenuID2";
            // 
            // MenuName
            // 
            this.MenuName.DataPropertyName = "MenuName";
            this.MenuName.HeaderText = "菜品名称";
            this.MenuName.Name = "MenuName";
            // 
            // Check
            // 
            this.Check.HeaderText = "选择";
            this.Check.Name = "Check";
            this.Check.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Check.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Check.Width = 62;
            // 
            // MenuID1
            // 
            this.MenuID1.DataPropertyName = "MenuID";
            this.MenuID1.HeaderText = "菜品编号";
            this.MenuID1.Name = "MenuID1";
            this.MenuID1.ReadOnly = true;
            this.MenuID1.Width = 80;
            // 
            // MenuName1
            // 
            this.MenuName1.DataPropertyName = "MenuName";
            this.MenuName1.HeaderText = "菜品名称";
            this.MenuName1.Name = "MenuName1";
            this.MenuName1.ReadOnly = true;
            this.MenuName1.Width = 120;
            // 
            // BianMa
            // 
            this.BianMa.DataPropertyName = "BianMa";
            this.BianMa.HeaderText = "拼音简码";
            this.BianMa.Name = "BianMa";
            this.BianMa.ReadOnly = true;
            this.BianMa.Visible = false;
            this.BianMa.Width = 80;
            // 
            // BigClassName1
            // 
            this.BigClassName1.DataPropertyName = "BigClassName";
            this.BigClassName1.HeaderText = "所属大类";
            this.BigClassName1.Name = "BigClassName1";
            this.BigClassName1.ReadOnly = true;
            this.BigClassName1.Width = 80;
            // 
            // SmallClassName1
            // 
            this.SmallClassName1.DataPropertyName = "SmallClassName";
            this.SmallClassName1.HeaderText = "所属小类";
            this.SmallClassName1.Name = "SmallClassName1";
            this.SmallClassName1.ReadOnly = true;
            this.SmallClassName1.Width = 80;
            // 
            // Unit
            // 
            this.Unit.DataPropertyName = "MenuUnit";
            this.Unit.HeaderText = "单位";
            this.Unit.Name = "Unit";
            this.Unit.ReadOnly = true;
            this.Unit.Width = 80;
            // 
            // NormalPrice1
            // 
            this.NormalPrice1.DataPropertyName = "NormalPrice";
            this.NormalPrice1.HeaderText = "正常价";
            this.NormalPrice1.Name = "NormalPrice1";
            this.NormalPrice1.ReadOnly = true;
            this.NormalPrice1.Width = 80;
            // 
            // MemberPrice1
            // 
            this.MemberPrice1.DataPropertyName = "MemberPrice";
            this.MemberPrice1.HeaderText = "会员价";
            this.MemberPrice1.Name = "MemberPrice1";
            this.MemberPrice1.ReadOnly = true;
            this.MemberPrice1.Width = 80;
            // 
            // Form4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(797, 433);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Form4";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "设置不参与统计的菜品";
            this.Load += new System.EventHandler(this.Form4_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.panel1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Paichu)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Panel panel1;
        private CCWin.SkinControl.SkinComboBox cmb_SmallClass;
        private CCWin.SkinControl.SkinComboBox cmb_BigClass;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private CCWin.SkinControl.SkinButton skinButton1;
        private CCWin.SkinControl.SkinButton btn_Querty;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox4;
        private CCWin.SkinControl.SkinWaterTextBox skinWaterTextBox1;
        private CCWin.SkinControl.SkinButton skinButton2;
        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.DataGridView dgv_Paichu;
        private System.Windows.Forms.DataGridViewTextBoxColumn MenuID2;
        private System.Windows.Forms.DataGridViewTextBoxColumn MenuName;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Check;
        private System.Windows.Forms.DataGridViewTextBoxColumn MenuID1;
        private System.Windows.Forms.DataGridViewTextBoxColumn MenuName1;
        private System.Windows.Forms.DataGridViewTextBoxColumn BianMa;
        private System.Windows.Forms.DataGridViewTextBoxColumn BigClassName1;
        private System.Windows.Forms.DataGridViewTextBoxColumn SmallClassName1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Unit;
        private System.Windows.Forms.DataGridViewTextBoxColumn NormalPrice1;
        private System.Windows.Forms.DataGridViewTextBoxColumn MemberPrice1;
    }
}