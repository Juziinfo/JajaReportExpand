﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace JajaRun
{
    public partial class FormMani : CCWin.CCSkinMain
    {
        public FormMani()
        {
            InitializeComponent();
        }

        private void FormMani_Load(object sender, EventArgs e)
        {

            skinPanel2.Location = new Point(this.Width / 2 - skinPanel2.Width / 2,(this.Height  -skinPanel1.Height)/2 -  skinPanel2.Height/2);




            LoadZhangTaoS();
        }

        public void LoadZhangTaoS()
        {


            IniHelper ini = new IniHelper(Application.StartupPath + "\\ConDB.ini");
            System.Collections.Specialized.StringCollection sectionlist = new System.Collections.Specialized.StringCollection();
            ini.ReadSections(sectionlist);


            //清空列表
            cmb_Zhangtao.Items.Clear();
            foreach (var s in sectionlist)
            {
                if (s !="当前账套")
                {

                    cmb_Zhangtao.Items.Add(s);
                }
               

            }







        }

        private void btn_NewZhangTao_Click(object sender, EventArgs e)
        {
            Form5 frm = new Form5();
            frm.Owner = this;
            frm.Show();
        }

        private void skinButton1_Click(object sender, EventArgs e)
        {
            IniHelper ini = new IniHelper(Application.StartupPath + "\\ConDB.ini");
            Connection.ZhangTaoName = cmb_Zhangtao.Text ;
            Connection.Server = ini.ReadIniData(cmb_Zhangtao.Text, "Server", ".");
            Connection.DataBase = ini.ReadIniData(cmb_Zhangtao.Text, "Database", "JajaCy01");
            Connection.User = ini.ReadIniData(cmb_Zhangtao.Text, "UserID", "sa");
            Connection.Password = ini.ReadIniData(cmb_Zhangtao.Text, "Password", "");





            if (Connection.TestConn(Connection.GetConnectionString()))
            {
                //修改目标程序的配置文件
                string FilePath = System.Windows.Forms.Application.StartupPath + "\\JajaReportExpand.exe.config";
                Connection.SetValue("sql_constr", Connection.GetConnectionString(), FilePath);

                //记录当前账套名称
                ini.WriteIniData("当前账套", "账套", Connection.ZhangTaoName, ini.IniFilePath);



                //启动目标程序              
                Process proc = new Process();
                proc.StartInfo.FileName = Application.StartupPath + "\\JajaReportExpand.exe";
                proc.Start();
            }

            else

            {
                CCWin.MessageBoxEx.Show("连接不到服务器！ 请确认目标服务器是否启动！");
            }
        }
    }
}
