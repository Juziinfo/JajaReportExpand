﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Xml;

namespace JajaRun
{
    public class Connection
    {
        public static string ZhangTaoName { set; get; }
        public static string Server { set; get; }
        public static string User { set; get; }
        public static string Password { set; get; }
        public static string DataBase { set; get; }



        //报表连接字符串
        public static  string GetReportConnectionString()
        {
            string connstr = string.Format(" Provider = SQLOLEDB.1; Password = {3}; Persist Security Info = True; User ID = {2}; Initial Catalog = {1}; Data Source ={0}; Use Procedure for Prepare = 1; Auto Translate = True; Packet Size = 4096; Workstation ID = JZSERVER; Use Encryption for Data = False; Tag with column collation when possible = False", Server, DataBase, User, Password);

            return connstr;

        }


        /// <summary>
        /// 数据库连接字符串
        /// </summary>
        /// <returns></returns>
        public static string GetConnectionString()
        {

            string constr = String.Format("Server={0};Database={1};User ID={2};Pwd={3}", Connection.Server, Connection.DataBase, Connection.User, Connection.Password);

            return constr;
        }


        //测试连接
        public static bool TestConn(string constr)
        {


            try
            {
                using (SqlConnection conn = new SqlConnection(constr))
                {
                    conn.Open();
                }
                return true;
            }
            catch (Exception)
            {

                return false;
            }


        }


        public static void SetValue(string constrName, string connectionString, string configFilePath)
        {
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(configFilePath);

            System.Xml.XmlNode xNode;
            //System.Xml.XmlElement xElem1;
            System.Xml.XmlElement xElem2;
            //System.Xml.XmlElement xElem3;
            xNode = xDoc.SelectSingleNode("//connectionStrings");


            XmlNodeList NodeList = xNode.SelectNodes("//add");
            if (NodeList.Count <= 0) //如果没有找到Add的节点 也就是没有任何连接字符串 则添加
            {
                xElem2 = xDoc.CreateElement("add");
                xElem2.SetAttribute("name", constrName);
                xElem2.SetAttribute("connectionString", connectionString);
                //providerName="System.Data.SqlClient"
                xElem2.SetAttribute("providerName", "System.Data.SqlClient");
                xNode.AppendChild(xElem2);

            }
            else //否则 开始查找连接字符串Name=指定名称的
            {
                foreach (System.Xml.XmlElement n in NodeList)
                {

                    if (n.GetAttribute("name") == constrName)
                    {
                        n.SetAttribute("connectionString", connectionString);

                    }
                    else //不存在 则创建一个新的
                    {
                        xElem2 = xDoc.CreateElement("add");
                        xElem2.SetAttribute("name", constrName);
                        xElem2.SetAttribute("connectionString", connectionString);
                        //providerName="System.Data.SqlClient"
                        xElem2.SetAttribute("providerName", "System.Data.SqlClient");
                        xNode.AppendChild(xElem2);
                    }

                }


            }
             

            xDoc.Save(configFilePath);
   
        }



    }
}
