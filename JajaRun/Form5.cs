﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;

using System.Data.SqlClient;
using CCWin;

namespace JajaRun
{
    public partial class Form5 : CCWin.CCSkinMain 
    {
        public Form5()
        {
            InitializeComponent();
        }

        private void Form5_Load(object sender, EventArgs e)
        {

        }

        private void skinButton1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace (txt_ZhangTao .Text) )
            {
                MessageBoxEx.Show("请输入账套名称！");
                txt_ZhangTao.Focus();
            
            }


           else if (string.IsNullOrWhiteSpace(txt_server .Text))
            {
                MessageBoxEx.Show("请输入服务器名称！");
                txt_server .Focus();

            }
           else  if (string.IsNullOrWhiteSpace(txt_Database.Text))
            {
                MessageBoxEx.Show("请输入数据库名称！");
                txt_Database.Focus();

            }
            else if (string.IsNullOrWhiteSpace(txt_UserID.Text))
            {
                MessageBoxEx.Show("请输入用户名！");
                txt_UserID.Focus();

            }
            else
            {

                Connection.DataBase = txt_Database.Text;
                Connection.Server = txt_server.Text;
                Connection.User = txt_UserID.Text;
                Connection.Password = txt_Password.Text;


               string  constr = String.Format("Server={0};Database={1};User ID={2};Pwd={3}", Connection.Server, Connection.DataBase, Connection.User, Connection.Password);
                if (Connection .TestConn(constr))
                {
                    //连接成功 写入配置文件
                    WriterDbCon();

                    FormMani frm = new FormMani();
                    frm =(FormMani ) this.Owner;

                    frm.LoadZhangTaoS();


                    this.Close();                    
                }
                else
                {

                    if (MessageBoxEx.Show("连接失败！是否继续保存？", "提示", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        WriterDbCon();
                      CCWin.   MessageBoxEx.Show("保存成功！");

                    }
                }


            }
      

        }


        private void WriterDbCon()
        {
            //连接成功！写入配置文件
            IniHelper ini = new IniHelper(Application.StartupPath + "\\ConDB.ini");
            ini.WriteIniData(txt_ZhangTao.Text.Trim(), "ZhangTaoName", txt_ZhangTao.Text.Trim(), ini.IniFilePath);
            ini.WriteIniData(txt_ZhangTao.Text.Trim(), "Server", txt_server.Text.Trim(), ini.IniFilePath);
            ini.WriteIniData(txt_ZhangTao.Text.Trim(), "Database", txt_Database.Text.Trim(), ini.IniFilePath);
            ini.WriteIniData(txt_ZhangTao.Text.Trim(), "UserID", txt_UserID.Text.Trim(), ini.IniFilePath);
            ini.WriteIniData(txt_ZhangTao.Text.Trim(), "Password", txt_Password.Text.Trim(), ini.IniFilePath);

          
        }
       

        private void AccessAppSettings()
        {
            //获取Configuration对象
             Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            //根据Key读取<add>元素的Value
            string name = config.AppSettings.Settings["name"].Value;

            //写入<add>元素的Value
            config.AppSettings.Settings["name"].Value = "xieyc";
            //增加<add>元素
            config.AppSettings.Settings.Add("url", "http://www.xieyc.com");
            //删除<add>元素
            config.AppSettings.Settings.Remove("name");
            //一定要记得保存，写不带参数的config.Save()也可以
            config.Save(ConfigurationSaveMode.Modified);
            //刷新，否则程序读取的还是之前的值（可能已装入内存）
            System.Configuration.ConfigurationManager.RefreshSection("appSettings");
        }

        private void skinButton2_Click(object sender, EventArgs e)
        {

            Connection.DataBase = txt_Database.Text;
            Connection.Server = txt_server.Text;
            Connection.User = txt_UserID.Text;
            Connection.Password = txt_Password.Text;


            string constr = String.Format("Server={0};Database={1};User ID={2};Pwd={3}", Connection.Server, Connection.DataBase ,Connection.User ,Connection.Password );
            
           
            try
            {
                using (SqlConnection conn = new SqlConnection(constr))
                {
                    conn.Open();
                }
                CCWin.MessageBoxEx.Show("连接成功！");
            }
            catch (Exception ex)
            {

                MessageBox.Show("连接失败：" + ex.Message);
            }
            
        }
    }
}
