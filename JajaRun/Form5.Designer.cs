﻿namespace JajaRun
{
    partial class Form5
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.skinButton2 = new CCWin.SkinControl.SkinButton();
            this.txt_Password = new CCWin.SkinControl.SkinTextBox();
            this.txt_UserID = new CCWin.SkinControl.SkinTextBox();
            this.txt_Database = new CCWin.SkinControl.SkinTextBox();
            this.txt_server = new CCWin.SkinControl.SkinTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_ZhangTao = new CCWin.SkinControl.SkinTextBox();
            this.skinButton1 = new CCWin.SkinControl.SkinButton();
            this.skinButton3 = new CCWin.SkinControl.SkinButton();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.skinButton2);
            this.groupBox1.Controls.Add(this.txt_Password);
            this.groupBox1.Controls.Add(this.txt_UserID);
            this.groupBox1.Controls.Add(this.txt_Database);
            this.groupBox1.Controls.Add(this.txt_server);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(34, 85);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Size = new System.Drawing.Size(394, 199);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "数据库连接信息";
            // 
            // skinButton2
            // 
            this.skinButton2.BackColor = System.Drawing.Color.Transparent;
            this.skinButton2.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(183)))), ((int)(((byte)(93)))));
            this.skinButton2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(183)))), ((int)(((byte)(93)))));
            this.skinButton2.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinButton2.DownBack = null;
            this.skinButton2.DownBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(178)))), ((int)(((byte)(255)))));
            this.skinButton2.ForeColor = System.Drawing.Color.White;
            this.skinButton2.InnerBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(157)))), ((int)(((byte)(154)))));
            this.skinButton2.IsDrawBorder = false;
            this.skinButton2.IsDrawGlass = false;
            this.skinButton2.IsEnabledDraw = false;
            this.skinButton2.Location = new System.Drawing.Point(302, 151);
            this.skinButton2.MouseBack = null;
            this.skinButton2.MouseBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(178)))), ((int)(((byte)(255)))));
            this.skinButton2.Name = "skinButton2";
            this.skinButton2.NormlBack = null;
            this.skinButton2.Size = new System.Drawing.Size(69, 28);
            this.skinButton2.TabIndex = 3;
            this.skinButton2.Text = "测试连接";
            this.skinButton2.UseVisualStyleBackColor = false;
            this.skinButton2.Click += new System.EventHandler(this.skinButton2_Click);
            // 
            // txt_Password
            // 
            this.txt_Password.BackColor = System.Drawing.Color.Transparent;
            this.txt_Password.DownBack = null;
            this.txt_Password.Icon = null;
            this.txt_Password.IconIsButton = false;
            this.txt_Password.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.txt_Password.IsPasswordChat = '\0';
            this.txt_Password.IsSystemPasswordChar = false;
            this.txt_Password.Lines = new string[0];
            this.txt_Password.Location = new System.Drawing.Point(110, 151);
            this.txt_Password.Margin = new System.Windows.Forms.Padding(0);
            this.txt_Password.MaxLength = 32767;
            this.txt_Password.MinimumSize = new System.Drawing.Size(28, 28);
            this.txt_Password.MouseBack = null;
            this.txt_Password.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.txt_Password.Multiline = false;
            this.txt_Password.Name = "txt_Password";
            this.txt_Password.NormlBack = null;
            this.txt_Password.Padding = new System.Windows.Forms.Padding(5);
            this.txt_Password.ReadOnly = false;
            this.txt_Password.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txt_Password.Size = new System.Drawing.Size(185, 28);
            // 
            // 
            // 
            this.txt_Password.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_Password.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt_Password.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.txt_Password.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.txt_Password.SkinTxt.Name = "BaseText";
            this.txt_Password.SkinTxt.Size = new System.Drawing.Size(175, 18);
            this.txt_Password.SkinTxt.TabIndex = 0;
            this.txt_Password.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.txt_Password.SkinTxt.WaterText = "密码";
            this.txt_Password.TabIndex = 2;
            this.txt_Password.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txt_Password.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.txt_Password.WaterText = "密码";
            this.txt_Password.WordWrap = true;
            // 
            // txt_UserID
            // 
            this.txt_UserID.BackColor = System.Drawing.Color.Transparent;
            this.txt_UserID.DownBack = null;
            this.txt_UserID.Icon = null;
            this.txt_UserID.IconIsButton = false;
            this.txt_UserID.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.txt_UserID.IsPasswordChat = '\0';
            this.txt_UserID.IsSystemPasswordChar = false;
            this.txt_UserID.Lines = new string[] {
        "sa"};
            this.txt_UserID.Location = new System.Drawing.Point(110, 111);
            this.txt_UserID.Margin = new System.Windows.Forms.Padding(0);
            this.txt_UserID.MaxLength = 32767;
            this.txt_UserID.MinimumSize = new System.Drawing.Size(28, 28);
            this.txt_UserID.MouseBack = null;
            this.txt_UserID.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.txt_UserID.Multiline = false;
            this.txt_UserID.Name = "txt_UserID";
            this.txt_UserID.NormlBack = null;
            this.txt_UserID.Padding = new System.Windows.Forms.Padding(5);
            this.txt_UserID.ReadOnly = false;
            this.txt_UserID.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txt_UserID.Size = new System.Drawing.Size(185, 28);
            // 
            // 
            // 
            this.txt_UserID.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_UserID.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt_UserID.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.txt_UserID.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.txt_UserID.SkinTxt.Name = "BaseText";
            this.txt_UserID.SkinTxt.Size = new System.Drawing.Size(175, 18);
            this.txt_UserID.SkinTxt.TabIndex = 0;
            this.txt_UserID.SkinTxt.Text = "sa";
            this.txt_UserID.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.txt_UserID.SkinTxt.WaterText = "用户名";
            this.txt_UserID.TabIndex = 2;
            this.txt_UserID.Text = "sa";
            this.txt_UserID.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txt_UserID.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.txt_UserID.WaterText = "用户名";
            this.txt_UserID.WordWrap = true;
            // 
            // txt_Database
            // 
            this.txt_Database.BackColor = System.Drawing.Color.Transparent;
            this.txt_Database.DownBack = null;
            this.txt_Database.Icon = null;
            this.txt_Database.IconIsButton = false;
            this.txt_Database.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.txt_Database.IsPasswordChat = '\0';
            this.txt_Database.IsSystemPasswordChar = false;
            this.txt_Database.Lines = new string[] {
        "JajaCy01"};
            this.txt_Database.Location = new System.Drawing.Point(110, 71);
            this.txt_Database.Margin = new System.Windows.Forms.Padding(0);
            this.txt_Database.MaxLength = 32767;
            this.txt_Database.MinimumSize = new System.Drawing.Size(28, 28);
            this.txt_Database.MouseBack = null;
            this.txt_Database.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.txt_Database.Multiline = false;
            this.txt_Database.Name = "txt_Database";
            this.txt_Database.NormlBack = null;
            this.txt_Database.Padding = new System.Windows.Forms.Padding(5);
            this.txt_Database.ReadOnly = false;
            this.txt_Database.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txt_Database.Size = new System.Drawing.Size(185, 28);
            // 
            // 
            // 
            this.txt_Database.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_Database.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt_Database.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.txt_Database.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.txt_Database.SkinTxt.Name = "BaseText";
            this.txt_Database.SkinTxt.Size = new System.Drawing.Size(175, 18);
            this.txt_Database.SkinTxt.TabIndex = 0;
            this.txt_Database.SkinTxt.Text = "JajaCy01";
            this.txt_Database.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.txt_Database.SkinTxt.WaterText = "数据库名称";
            this.txt_Database.TabIndex = 2;
            this.txt_Database.Text = "JajaCy01";
            this.txt_Database.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txt_Database.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.txt_Database.WaterText = "数据库名称";
            this.txt_Database.WordWrap = true;
            // 
            // txt_server
            // 
            this.txt_server.BackColor = System.Drawing.Color.Transparent;
            this.txt_server.DownBack = null;
            this.txt_server.Icon = null;
            this.txt_server.IconIsButton = false;
            this.txt_server.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.txt_server.IsPasswordChat = '\0';
            this.txt_server.IsSystemPasswordChar = false;
            this.txt_server.Lines = new string[] {
        "."};
            this.txt_server.Location = new System.Drawing.Point(110, 31);
            this.txt_server.Margin = new System.Windows.Forms.Padding(0);
            this.txt_server.MaxLength = 32767;
            this.txt_server.MinimumSize = new System.Drawing.Size(28, 28);
            this.txt_server.MouseBack = null;
            this.txt_server.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.txt_server.Multiline = false;
            this.txt_server.Name = "txt_server";
            this.txt_server.NormlBack = null;
            this.txt_server.Padding = new System.Windows.Forms.Padding(5);
            this.txt_server.ReadOnly = false;
            this.txt_server.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txt_server.Size = new System.Drawing.Size(185, 28);
            // 
            // 
            // 
            this.txt_server.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_server.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt_server.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.txt_server.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.txt_server.SkinTxt.Name = "BaseText";
            this.txt_server.SkinTxt.Size = new System.Drawing.Size(175, 18);
            this.txt_server.SkinTxt.TabIndex = 0;
            this.txt_server.SkinTxt.Text = ".";
            this.txt_server.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.txt_server.SkinTxt.WaterText = "服务器地址";
            this.txt_server.TabIndex = 2;
            this.txt_server.Text = ".";
            this.txt_server.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txt_server.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.txt_server.WaterText = "服务器地址";
            this.txt_server.WordWrap = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(61, 151);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 21);
            this.label5.TabIndex = 1;
            this.label5.Text = "密码:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(45, 111);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 21);
            this.label4.TabIndex = 1;
            this.label4.Text = "用户名:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(13, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 21);
            this.label3.TabIndex = 1;
            this.label3.Text = "数据库名称:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(13, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 21);
            this.label2.TabIndex = 1;
            this.label2.Text = "服务器地址:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(63, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 21);
            this.label1.TabIndex = 1;
            this.label1.Text = "账套名称:";
            // 
            // txt_ZhangTao
            // 
            this.txt_ZhangTao.BackColor = System.Drawing.Color.Transparent;
            this.txt_ZhangTao.DownBack = null;
            this.txt_ZhangTao.Icon = null;
            this.txt_ZhangTao.IconIsButton = false;
            this.txt_ZhangTao.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.txt_ZhangTao.IsPasswordChat = '\0';
            this.txt_ZhangTao.IsSystemPasswordChar = false;
            this.txt_ZhangTao.Lines = new string[0];
            this.txt_ZhangTao.Location = new System.Drawing.Point(144, 50);
            this.txt_ZhangTao.Margin = new System.Windows.Forms.Padding(0);
            this.txt_ZhangTao.MaxLength = 32767;
            this.txt_ZhangTao.MinimumSize = new System.Drawing.Size(28, 28);
            this.txt_ZhangTao.MouseBack = null;
            this.txt_ZhangTao.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.txt_ZhangTao.Multiline = false;
            this.txt_ZhangTao.Name = "txt_ZhangTao";
            this.txt_ZhangTao.NormlBack = null;
            this.txt_ZhangTao.Padding = new System.Windows.Forms.Padding(5);
            this.txt_ZhangTao.ReadOnly = false;
            this.txt_ZhangTao.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txt_ZhangTao.Size = new System.Drawing.Size(185, 28);
            // 
            // 
            // 
            this.txt_ZhangTao.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_ZhangTao.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt_ZhangTao.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.txt_ZhangTao.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.txt_ZhangTao.SkinTxt.Name = "BaseText";
            this.txt_ZhangTao.SkinTxt.Size = new System.Drawing.Size(175, 18);
            this.txt_ZhangTao.SkinTxt.TabIndex = 0;
            this.txt_ZhangTao.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.txt_ZhangTao.SkinTxt.WaterText = "账套名称";
            this.txt_ZhangTao.TabIndex = 2;
            this.txt_ZhangTao.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txt_ZhangTao.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.txt_ZhangTao.WaterText = "账套名称";
            this.txt_ZhangTao.WordWrap = true;
            // 
            // skinButton1
            // 
            this.skinButton1.BackColor = System.Drawing.Color.Transparent;
            this.skinButton1.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(175)))), ((int)(((byte)(154)))));
            this.skinButton1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(175)))), ((int)(((byte)(154)))));
            this.skinButton1.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinButton1.DownBack = null;
            this.skinButton1.DownBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(178)))), ((int)(((byte)(255)))));
            this.skinButton1.ForeColor = System.Drawing.Color.White;
            this.skinButton1.InnerBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(157)))), ((int)(((byte)(154)))));
            this.skinButton1.IsDrawBorder = false;
            this.skinButton1.IsDrawGlass = false;
            this.skinButton1.IsEnabledDraw = false;
            this.skinButton1.Location = new System.Drawing.Point(253, 291);
            this.skinButton1.MouseBack = null;
            this.skinButton1.MouseBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(178)))), ((int)(((byte)(255)))));
            this.skinButton1.Name = "skinButton1";
            this.skinButton1.NormlBack = null;
            this.skinButton1.Size = new System.Drawing.Size(76, 32);
            this.skinButton1.TabIndex = 3;
            this.skinButton1.Text = "保存";
            this.skinButton1.UseVisualStyleBackColor = false;
            this.skinButton1.Click += new System.EventHandler(this.skinButton1_Click);
            // 
            // skinButton3
            // 
            this.skinButton3.BackColor = System.Drawing.Color.Transparent;
            this.skinButton3.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(175)))), ((int)(((byte)(154)))));
            this.skinButton3.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(175)))), ((int)(((byte)(154)))));
            this.skinButton3.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinButton3.DownBack = null;
            this.skinButton3.DownBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(178)))), ((int)(((byte)(255)))));
            this.skinButton3.ForeColor = System.Drawing.Color.White;
            this.skinButton3.InnerBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(157)))), ((int)(((byte)(154)))));
            this.skinButton3.IsDrawBorder = false;
            this.skinButton3.IsDrawGlass = false;
            this.skinButton3.IsEnabledDraw = false;
            this.skinButton3.Location = new System.Drawing.Point(351, 291);
            this.skinButton3.MouseBack = null;
            this.skinButton3.MouseBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(178)))), ((int)(((byte)(255)))));
            this.skinButton3.Name = "skinButton3";
            this.skinButton3.NormlBack = null;
            this.skinButton3.Size = new System.Drawing.Size(77, 32);
            this.skinButton3.TabIndex = 4;
            this.skinButton3.Text = "取消";
            this.skinButton3.UseVisualStyleBackColor = false;
            // 
            // Form5
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(471, 330);
            this.Controls.Add(this.skinButton3);
            this.Controls.Add(this.skinButton1);
            this.Controls.Add(this.txt_ZhangTao);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form5";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "设置账套";
            this.Load += new System.EventHandler(this.Form5_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private CCWin.SkinControl.SkinTextBox txt_Password;
        private CCWin.SkinControl.SkinTextBox txt_UserID;
        private CCWin.SkinControl.SkinTextBox txt_Database;
        private CCWin.SkinControl.SkinTextBox txt_server;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private CCWin.SkinControl.SkinTextBox txt_ZhangTao;
        private CCWin.SkinControl.SkinButton skinButton2;
        private CCWin.SkinControl.SkinButton skinButton1;
        private CCWin.SkinControl.SkinButton skinButton3;
    }
}