﻿namespace JajaRun
{
    partial class FormMani
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMani));
            this.skinPanel1 = new CCWin.SkinControl.SkinPanel();
            this.skinPictureBox1 = new CCWin.SkinControl.SkinPictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.skinLine1 = new CCWin.SkinControl.SkinLine();
            this.skinPanel2 = new CCWin.SkinControl.SkinPanel();
            this.btn_NewZhangTao = new CCWin.SkinControl.SkinButton();
            this.btn_OK = new CCWin.SkinControl.SkinButton();
            this.cmb_Zhangtao = new CCWin.SkinControl.SkinComboBox();
            this.skinPanel3 = new CCWin.SkinControl.SkinPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.skinPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.skinPictureBox1)).BeginInit();
            this.skinPanel2.SuspendLayout();
            this.skinPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // skinPanel1
            // 
            this.skinPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(163)))), ((int)(((byte)(231)))));
            this.skinPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.skinPanel1.Controls.Add(this.skinPictureBox1);
            this.skinPanel1.Controls.Add(this.label2);
            this.skinPanel1.Controls.Add(this.skinLine1);
            this.skinPanel1.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.skinPanel1.DownBack = null;
            this.skinPanel1.Location = new System.Drawing.Point(4, 28);
            this.skinPanel1.MouseBack = null;
            this.skinPanel1.Name = "skinPanel1";
            this.skinPanel1.NormlBack = null;
            this.skinPanel1.Size = new System.Drawing.Size(702, 91);
            this.skinPanel1.TabIndex = 3;
            // 
            // skinPictureBox1
            // 
            this.skinPictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.skinPictureBox1.Image = global::JajaRun.Properties.Resources.png_0234;
            this.skinPictureBox1.Location = new System.Drawing.Point(49, 27);
            this.skinPictureBox1.Name = "skinPictureBox1";
            this.skinPictureBox1.Size = new System.Drawing.Size(63, 61);
            this.skinPictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.skinPictureBox1.TabIndex = 1;
            this.skinPictureBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(127, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 25);
            this.label2.TabIndex = 3;
            this.label2.Text = "提成报表";
            // 
            // skinLine1
            // 
            this.skinLine1.BackColor = System.Drawing.Color.Transparent;
            this.skinLine1.LineColor = System.Drawing.Color.White;
            this.skinLine1.LineHeight = 1;
            this.skinLine1.Location = new System.Drawing.Point(-9, 67);
            this.skinLine1.Name = "skinLine1";
            this.skinLine1.Size = new System.Drawing.Size(1920, 10);
            this.skinLine1.TabIndex = 2;
            this.skinLine1.Text = "skinLine1";
            // 
            // skinPanel2
            // 
            this.skinPanel2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.skinPanel2.BorderColor = System.Drawing.Color.DimGray;
            this.skinPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.skinPanel2.Controls.Add(this.btn_NewZhangTao);
            this.skinPanel2.Controls.Add(this.btn_OK);
            this.skinPanel2.Controls.Add(this.cmb_Zhangtao);
            this.skinPanel2.Controls.Add(this.skinPanel3);
            this.skinPanel2.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinPanel2.DownBack = null;
            this.skinPanel2.Location = new System.Drawing.Point(211, 233);
            this.skinPanel2.MouseBack = null;
            this.skinPanel2.Name = "skinPanel2";
            this.skinPanel2.NormlBack = null;
            this.skinPanel2.Size = new System.Drawing.Size(282, 180);
            this.skinPanel2.TabIndex = 4;
            // 
            // btn_NewZhangTao
            // 
            this.btn_NewZhangTao.BackColor = System.Drawing.Color.Transparent;
            this.btn_NewZhangTao.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(175)))), ((int)(((byte)(154)))));
            this.btn_NewZhangTao.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(175)))), ((int)(((byte)(154)))));
            this.btn_NewZhangTao.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_NewZhangTao.DownBack = null;
            this.btn_NewZhangTao.DownBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(178)))), ((int)(((byte)(255)))));
            this.btn_NewZhangTao.ForeColor = System.Drawing.Color.White;
            this.btn_NewZhangTao.InnerBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(157)))), ((int)(((byte)(154)))));
            this.btn_NewZhangTao.IsDrawBorder = false;
            this.btn_NewZhangTao.IsDrawGlass = false;
            this.btn_NewZhangTao.IsEnabledDraw = false;
            this.btn_NewZhangTao.Location = new System.Drawing.Point(41, 110);
            this.btn_NewZhangTao.MouseBack = null;
            this.btn_NewZhangTao.MouseBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(178)))), ((int)(((byte)(255)))));
            this.btn_NewZhangTao.Name = "btn_NewZhangTao";
            this.btn_NewZhangTao.NormlBack = null;
            this.btn_NewZhangTao.Size = new System.Drawing.Size(92, 32);
            this.btn_NewZhangTao.TabIndex = 2;
            this.btn_NewZhangTao.Text = "新建账套...";
            this.btn_NewZhangTao.UseVisualStyleBackColor = false;
            this.btn_NewZhangTao.Click += new System.EventHandler(this.btn_NewZhangTao_Click);
            // 
            // btn_OK
            // 
            this.btn_OK.BackColor = System.Drawing.Color.Transparent;
            this.btn_OK.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(175)))), ((int)(((byte)(154)))));
            this.btn_OK.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(175)))), ((int)(((byte)(154)))));
            this.btn_OK.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_OK.DownBack = null;
            this.btn_OK.DownBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(178)))), ((int)(((byte)(255)))));
            this.btn_OK.ForeColor = System.Drawing.Color.White;
            this.btn_OK.InnerBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(157)))), ((int)(((byte)(154)))));
            this.btn_OK.IsDrawBorder = false;
            this.btn_OK.IsDrawGlass = false;
            this.btn_OK.IsEnabledDraw = false;
            this.btn_OK.Location = new System.Drawing.Point(152, 110);
            this.btn_OK.MouseBack = null;
            this.btn_OK.MouseBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(178)))), ((int)(((byte)(255)))));
            this.btn_OK.Name = "btn_OK";
            this.btn_OK.NormlBack = null;
            this.btn_OK.Size = new System.Drawing.Size(92, 32);
            this.btn_OK.TabIndex = 2;
            this.btn_OK.Text = "确定";
            this.btn_OK.UseVisualStyleBackColor = false;
            this.btn_OK.Click += new System.EventHandler(this.skinButton1_Click);
            // 
            // cmb_Zhangtao
            // 
            this.cmb_Zhangtao.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cmb_Zhangtao.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cmb_Zhangtao.FormattingEnabled = true;
            this.cmb_Zhangtao.Location = new System.Drawing.Point(41, 52);
            this.cmb_Zhangtao.Name = "cmb_Zhangtao";
            this.cmb_Zhangtao.Size = new System.Drawing.Size(203, 30);
            this.cmb_Zhangtao.TabIndex = 1;
            this.cmb_Zhangtao.WaterText = "";
            // 
            // skinPanel3
            // 
            this.skinPanel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(163)))), ((int)(((byte)(231)))));
            this.skinPanel3.Controls.Add(this.label1);
            this.skinPanel3.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinPanel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.skinPanel3.DownBack = null;
            this.skinPanel3.Location = new System.Drawing.Point(0, 0);
            this.skinPanel3.MouseBack = null;
            this.skinPanel3.Name = "skinPanel3";
            this.skinPanel3.NormlBack = null;
            this.skinPanel3.Size = new System.Drawing.Size(280, 36);
            this.skinPanel3.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(280, 36);
            this.label1.TabIndex = 0;
            this.label1.Text = "选择账套";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FormMani
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(710, 472);
            this.Controls.Add(this.skinPanel2);
            this.Controls.Add(this.skinPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormMani";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "选择账套";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormMani_Load);
            this.skinPanel1.ResumeLayout(false);
            this.skinPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.skinPictureBox1)).EndInit();
            this.skinPanel2.ResumeLayout(false);
            this.skinPanel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private CCWin.SkinControl.SkinPanel skinPanel1;
        private CCWin.SkinControl.SkinPictureBox skinPictureBox1;
        private System.Windows.Forms.Label label2;
        private CCWin.SkinControl.SkinLine skinLine1;
        private CCWin.SkinControl.SkinPanel skinPanel2;
        private CCWin.SkinControl.SkinButton btn_NewZhangTao;
        private CCWin.SkinControl.SkinButton btn_OK;
        private CCWin.SkinControl.SkinComboBox cmb_Zhangtao;
        private CCWin.SkinControl.SkinPanel skinPanel3;
        private System.Windows.Forms.Label label1;
    }
}